from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns('',
    url(r'^$', index, name="index"),
    url(r'^(\d+)/$', detail, name="detail"),
    url(r'^search/$', search),
)