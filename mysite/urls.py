from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()

from.views import *

urlpatterns = patterns('',
    url(r'^hello/$', hello),
    url(r'^$', welcome),
    url(r'^time/$', current_datetime),
    url(r'^time/plus/(\d+)$', hours_ahead),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^add/(\d+)/(\d+)$', add),
    url(r'^subtract/(\d+)/(\d+)$', subtract),
    url(r'^(\d+)/(\d+)$', product),
    url(r'^(\d+)$', square),
    url(r'^meta/$', display_meta),
    url(r'^books/', include('books.urls')),
    url(r'^contact/$', contact),
    url(r'^thanks/$', thanks),
    url(r'^registration/$', registration),
    url(r'^login/$', login),
    url(r'^booklist/$', book_list),
)