import datetime

from django.template import Context
from django.template.loader import get_template
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import ContactForm, Registration, Login
from django.contrib.auth.decorators import login_required

def hello(request):
	return HttpResponse("Hello World")

# Create your views here.

def current_datetime(request):
  now = datetime.datetime.now()
  context = {'current_datetime': now}
  return render(
  		request,
  		'current_datetime.html',
  		context
  	)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()

    dt = {
    	'next_time': datetime.datetime.now() + datetime.timedelta(hours=offset),
    	'hour_offset': offset
    	}

    return render(request, 'hours_ahead.html', dt)

def welcome(request):
	return HttpResponse("Welcome to Math")

def add(request, a, b):
	a = int(a)
	b = int(b)
	answer = int(a + b)

	return HttpResponse("The sum of {} and {} is {}".format(a, b, answer))

def subtract(request, c, d):
	c = int(c)
	d = int(d)
	sans = int(c - d)

	return HttpResponse("The difference of {} and {} is {}".format(c, d, sans))

def product(request, e, f):
	e = int(e)
	f = int(f)
	mans = e * f

	return HttpResponse("The product of {} and {} is {}".format(e, f, mans))	

def square(request, g):
	g = int(g)

	return HttpResponse("The square of {} is {}".format(g, g*g))

def display_meta(request):
    values = request.META.items()
    values.sort()
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))

def contact(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = ContactForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_data.get('message')
            print subject
            print message
            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()  # An unbound form

    return render(request, 'contact.html', {'form': form})

def thanks(request):
  return HttpResponse("TENCHU")

def registration(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = Registration(request.POST)
        print form  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            form.save()
            return HttpResponseRedirect('/books/')
    else:
        form = Registration()  # An unbound form

    return render(request, 'registration.html', {'form': form})

@login_required
def login(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = Login(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            return HttpResponseRedirect('/booklist/')
        else:
            HttpResponse("User does not exist")
    else:
        form = Login()  # An unbound form

    return render(request, 'login.html', {'form': form})

def book_list(request):
    return render(request, 'booklist.html')


  